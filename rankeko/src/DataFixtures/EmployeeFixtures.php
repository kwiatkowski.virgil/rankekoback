<?php

namespace App\DataFixtures;
use App\Entity\Employee;
use App\Entity\Company;
use App\Controller\CompanyController;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EmployeeFixtures extends Fixture  
{
    public function load(ObjectManager $manager): void
    {
        $companies = $manager
        ->getRepository(Company::class)
        ->findAll();

    if (!$companies) {
        throw $this->createNotFoundException(
            'Pas de groupe'
        );
    };
        
        for ($i = 0; $i < 20; $i++) {
            $user = new Employee();
            $user->setFirstname('firstname '.$i);
            $user->setLastname('firstname '.$i);
            $user->setEmail('email '.$i);
            $user->setPassword('password '.$i);
            $user->setUserProfile('driver');
            $user->setPhoneNumber('0698989898');
            $user->setAvatar('url');
            $user->setPoints(54287);
            $manager->persist($user);
        }
        $manager->flush();
    }
}

