<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class CourseFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $date = strtotime('05/02/2022');
        $newformat = date('Y-m-d',$date);


        for ($i = 0; $i < 20; $i++) {
            $course = new Course();
            $course->setDistance($i);
            $course->setDeparturePoint('ici');
            $course->setArrivalPoint('là-bas');
            $course->setQRcode('icojnzusvhbjqlnpijh');
            $course->setCreatedAt(new \DateTime());
            $course->setFinishedAt(new \DateTime());
            $manager->persist($course);
        }
        $manager->flush();
    }
}
