<?php

// src/App/EventListener/JWTCreatedListener.php
namespace App\EventListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Security;

class JWTCreatedListener{

    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
    * @param JWTCreatedEvent $event
    *
    * @return void
    */

    public function onJWTCreated(JWTCreatedEvent $event)
    {

        $payload = $event->getData();
        $user = $this->security->getUser();
        $payload['id'] = $user->getId();

        $event->setData($payload);
        $header = $event->getHeader();
        $header['cty'] = 'JWT';
        $event->setHeader($header);
    }
}