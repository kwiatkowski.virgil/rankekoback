<?php

namespace App\DataFixtures;

use App\Entity\Vehicle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VehicleFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $vehicle = new Vehicle();
            $vehicle->setFuelType('Diesel');
            $vehicle->setCategory('Citadine');
            $vehicle->setYear('2012');

            $manager->persist($vehicle);
        }
        $manager->flush();
    }
}
