<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Employee;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $em;
    private $userPasswordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher){
        $this->em = $entityManager;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Employee;
    }

    public function persist($user, array $context = [])
    {
        // Encode le password dans les DATAS
        $encodedPassword = $this->userPasswordHasher->hashPassword(
            $user,
            $user->getPassword()
        );

        // Cas de création d'un utilisateur (POST)
        if(array_key_exists('collection_operation_name', $context) && $context["collection_operation_name"] == "post"){

            $user->setRoles(["ROLE_USER"]);         // INIT Role USER
            $user->setPassword($encodedPassword);    // INIT encode Password

        }

        // Cas de MAJ d'un utilisateur (PATCH)
        if(array_key_exists('item_operation_name', $context) && $context["item_operation_name"] == "patch"){
            // Check if password has changed between previous and now DATA
            if( $context["previous_data"]->getPassword() != $user->getPassword()){
                $user->setPassword($encodedPassword); // Set If diff
            }
            $user->setRoles($context["previous_data"]->getRoles());         // INIT Role USER
        }

        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }
    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}