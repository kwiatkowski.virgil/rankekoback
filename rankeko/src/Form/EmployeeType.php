<?php

namespace App\Form;

use App\Entity\Employee;
use App\Entity\Company;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('userProfile')
            ->add('phoneNumber')
            ->add('email')
            ->add('password')
            ->add('avatar')
            ->add('points')
            ->add('label')
            ->add('company',EntityType::class, [
                'class' => Company::class,
                'choice_label' => 'id',
                'multiple' => false,
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
