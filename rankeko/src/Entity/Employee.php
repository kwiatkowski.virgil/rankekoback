<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [ "security" => "is_granted('ROLE_USER')"],
        "post",
        
    ],
    itemOperations: [   
        "get" => [ "security" => "is_granted('ROLE_USER')"],
        "patch" => [ "security" => "is_granted('ROLE_ADMIN') or (object.id == user.id)"],
     ],
    normalizationContext : ['groups'=>["read:collection","read:Vehicle","read:Company","read:Course"]],
)]

class Employee implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["read:collection"])]

    public $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read:collection"])]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read:collection"])]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection'])]
    private $userProfile;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection'])]
    private $phoneNumber;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection'])]
    private $email;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:employee'])]
    private $password;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read:collection'])]
    private $avatar;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['read:collection'])]
    private $points;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read:collection'])]
    private $label;

    #[ORM\ManyToMany(targetEntity: Course::class, mappedBy: 'employe')]
    #[Groups(['read:collection'])]
    private $courses;

    #[ORM\OneToMany(mappedBy: 'employe', targetEntity: Vehicle::class)]
    #[Groups(['read:collection'])]
    private $vehicles;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'employees')]
    #[Groups(['read:collection'])]
    private $company;

    private $roles;
    #[Groups(['read:collection'])]
    #[ORM\Column(type: 'boolean')]
    private $account_confirmed;

    #[Groups(['read:collection'])]
    #[ORM\Column(type: 'boolean')]
    private $showTutorial;
    


    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUserProfile(): ?string
    {
        return $this->userProfile;
    }

    public function setUserProfile(string $userProfile): self
    {
        $this->userProfile = $userProfile;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(?int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->addEmploye($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            $course->removeEmploye($this);
        }

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->setEmploye($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getEmploye() === $this) {
                $vehicle->setEmploye(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
    /**
     * The public representation of the user (e.g. a username, an email address, etc.)
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }
    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getAccountConfirmed(): ?bool
    {
        return $this->account_confirmed;
    }

    public function setAccountConfirmed(bool $account_confirmed): self
    {
        $this->account_confirmed = $account_confirmed;

        return $this;
    }

    public function getShowTutorial(): ?bool
    {
        return $this->showTutorial;
    }

    public function setShowTutorial(bool $showTutorial): self
    {
        $this->showTutorial = $showTutorial;

        return $this;
    }
}
