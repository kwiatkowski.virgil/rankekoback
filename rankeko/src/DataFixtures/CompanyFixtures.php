<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $company = new Company();
            $company->setName('company '.$i);
            $company->setLabel('A');
            $company->setEmail('entreprise'.$i.'@gmail.com');
            $company->setPoints(54287);
            $company->setPassword('password '.$i);

            $manager->persist($company);
        }
        $manager->flush();
    }
}
