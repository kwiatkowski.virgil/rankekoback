<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220210095017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, label VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, distance INT DEFAULT NULL, departure_point VARCHAR(255) DEFAULT NULL, arrival_point VARCHAR(255) DEFAULT NULL, qrcode VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, finished_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_employee (course_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_F42B18F7591CC992 (course_id), INDEX IDX_F42B18F78C03F15C (employee_id), PRIMARY KEY(course_id, employee_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, user_profile VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, points INT DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, INDEX IDX_5D9F75A1979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle (id INT AUTO_INCREMENT NOT NULL, employe_id INT DEFAULT NULL, fuel_type VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, year VARCHAR(255) NOT NULL, INDEX IDX_1B80E4861B65292 (employe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course_employee ADD CONSTRAINT FK_F42B18F7591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_employee ADD CONSTRAINT FK_F42B18F78C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4861B65292 FOREIGN KEY (employe_id) REFERENCES employee (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1979B1AD6');
        $this->addSql('ALTER TABLE course_employee DROP FOREIGN KEY FK_F42B18F7591CC992');
        $this->addSql('ALTER TABLE course_employee DROP FOREIGN KEY FK_F42B18F78C03F15C');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E4861B65292');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE course_employee');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE vehicle');
    }
}
