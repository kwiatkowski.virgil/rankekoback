<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;


#[ORM\Entity(repositoryClass: VehicleRepository::class)]
#[ApiResource]

class Vehicle 

{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["read:Vehicle"])]

    private $id;

    #[ORM\ManyToOne(targetEntity: Employee::class, inversedBy: 'vehicles')]
    private $employe;
    #[Groups(["read:Vehicle"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $brand;
    #[Groups(["read:Vehicle"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $fuelType;
    #[Groups(["read:Vehicle"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $model;
    #[Groups(["read:Vehicle"])]
    #[ORM\Column(type: 'integer', length: 255)]
    private $Co2Emissions;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFuelType(): ?string
    {
        return $this->fuelType;
    }

    public function setFuelType(string $fuelType): self
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    public function getEmploye(): ?Employee
    {
        return $this->employe;
    }

    public function setEmploye(?Employee $employe): self
    {
        $this->employe = $employe;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getCo2Emissions(): ?string
    {
        return $this->Co2Emissions;
    }

    public function setCo2Emissions(string $Co2Emissions): self
    {
        $this->Co2Emissions = $Co2Emissions;

        return $this;
    }
}
